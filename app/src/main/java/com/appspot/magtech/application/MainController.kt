package com.appspot.magtech.application

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.ui.connection.FOREGROUND_ID

class MainController(private val networkUtils: NetworkUtils,
                     private val serviceStarter: ServiceStarter,
                     notificationsWorker: NotificationsWorker
) {

    var openStatus by ObservableVar(false) {
        notificationsWorker.isNotificationExists(FOREGROUND_ID)
    }
    var currentIp by ObservableVar("")

    fun changeServerStatus() {
        if (!openStatus) {
            startServer()
        } else {
            stopServer()
        }
    }

    private fun stopServer() {
        openStatus = false
        serviceStarter.stopService()
    }

    private fun startServer() {
        openStatus = true
        currentIp = networkUtils.getIPAddress(true)
        serviceStarter.startService()
    }
}