package com.appspot.magtech.application

interface NetworkUtils {

    fun getMACAddress(interfaceName: String?): String

    fun getIPAddress(useIPv4: Boolean): String
}