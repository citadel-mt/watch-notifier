package com.appspot.magtech.application

interface NotificationsWorker {

    fun isNotificationExists(id: Int): Boolean
}