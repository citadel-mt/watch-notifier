package com.appspot.magtech.application

interface ServiceStarter {

    fun startService(vararg params: Pair<String, Any?>)

    fun stopService()
}