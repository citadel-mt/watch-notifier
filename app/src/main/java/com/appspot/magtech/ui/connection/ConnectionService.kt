package com.appspot.magtech.ui.connection

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.support.annotation.RequiresApi
import com.appspot.magtech.R
import com.github.salomonbrys.kotson.jsonObject

const val PORT = 12345
const val FOREGROUND_ID = 777
const val CHANNEL_ID = "seneschal_android_channel_01"
const val CHANNEL_NAME = "Seneschal Notification Listener"
const val STOP_COMMAND = "com.appspot.magtech.seneschal_android.action_delete"

    class ConnectionService: NotificationListenerService() {

    private var flagRunning = false
    private val socketServer =
        NotificationSocketServer(PORT)

    override fun onDestroy() {
        flagRunning = false
        socketServer.stop()
        println("Service destroyed")
        super.onDestroy()
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        socketServer.broadcast(jsonObject(
            "package" to sbn.packageName,
            "title" to sbn.notification.extras[Notification.EXTRA_TITLE],
            "text" to sbn.notification.extras[Notification.EXTRA_TEXT]
        ).toString())
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action == STOP_COMMAND){
            stopSelf()
        }
        println("Start")
        flagRunning = true
        startForeground(FOREGROUND_ID, createNotification("Connected devices: 0"))

        socketServer.start()
        println("Server started on port: 12345")
        return super.onStartCommand(intent, flags, startId)
    }

    private fun createNotification(text: String): Notification {
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(CHANNEL_ID, CHANNEL_NAME)
            Notification.Builder(this, CHANNEL_ID)
        } else {
            Notification.Builder(this)
        }

        builder.setSmallIcon(R.drawable.icon)
            .setContentTitle(getString(R.string.notification_title))
            .setContentText(text)
        return builder.build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT)
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onBind(intent: Intent?): IBinder? {
        return super.onBind(intent)
    }
}