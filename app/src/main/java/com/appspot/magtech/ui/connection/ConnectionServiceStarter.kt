package com.appspot.magtech.ui.connection

import android.content.Context
import com.appspot.magtech.application.ServiceStarter
import org.jetbrains.anko.startService
import org.jetbrains.anko.stopService

class ConnectionServiceStarter(private val context: Context): ServiceStarter {

    override fun startService(vararg params: Pair<String, Any?>) {
        context.startService<ConnectionService>()
    }

    override fun stopService() {
        context.stopService<ConnectionService>()
    }
}