package com.appspot.magtech.ui.connection

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.net.InetSocketAddress

class NotificationSocketServer(port: Int): WebSocketServer(InetSocketAddress(port)) {

    override fun onOpen(conn: WebSocket?, handshake: ClientHandshake?) {
        conn?.send("Welcome to the server!")
        println("${conn?.remoteSocketAddress?.address?.hostAddress} entered the room!")
    }

    override fun onStart() {
        println("Server started!")
    }

    override fun onMessage(conn: WebSocket?, message: String?) {
        System.out.println("$conn: $message")
        conn?.send("Reply from server: $message")
    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        ex?.printStackTrace()
    }

    override fun onClose(conn: WebSocket?, code: Int, reason: String?, remote: Boolean) {
        println("$conn has left the room!")
    }

    override fun stop() {
        broadcast("Servers is stopped")
        super.stop()
    }
}