package com.appspot.magtech.ui.main

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Gravity
import com.appspot.magtech.R
import com.appspot.magtech.application.MainController
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.bind
import com.appspot.magtech.ui.connection.ConnectionServiceStarter
import com.appspot.magtech.utils.AndroidNetworkUtils
import com.appspot.magtech.utils.SimpleNotificationsWorker
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    private val ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners"

    private val networkUtils = AndroidNetworkUtils()
    private val serviceStarter = ConnectionServiceStarter(this)
    private val notificationsWorker = SimpleNotificationsWorker(this)

    /*override val dkodein = Kodein.direct {
        bind<NetworkUtils>() with singleton { AndroidNetworkUtils() }
        bind<ServiceStarter>() with singleton {
            ConnectionServiceStarter(this@MainActivity)
        }
        bind<NotificationsWorker>() with singleton {
            SimpleNotificationsWorker(this@MainActivity)
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mainView = MainView(
            MainController(networkUtils, serviceStarter, notificationsWorker)
        )
        mainView.setContentView(this)

        if(!isNotificationServiceEnabled()) {
            val enableNotificationListenerAlertDialog = buildNotificationServiceAlertDialog()
            enableNotificationListenerAlertDialog.show()
        }
    }

    private fun isNotificationServiceEnabled(): Boolean {
        val pkgName = packageName
        val flat = Settings.Secure.getString(
            contentResolver,
            ENABLED_NOTIFICATION_LISTENERS
        )
        if (!TextUtils.isEmpty(flat)) {
            val names = flat.split(":".toRegex()).dropLastWhile { it.isEmpty() } .toTypedArray()
            for (i in names.indices) {
                val cn = ComponentName.unflattenFromString(names[i])
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.packageName)) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun buildNotificationServiceAlertDialog(): AlertDialog {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(R.string.notification_listener_service)
        alertDialogBuilder.setMessage(R.string.notification_listener_service_explanation)
        alertDialogBuilder.setPositiveButton(
            R.string.yes
        ) { dialog, id -> startActivity(Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS)) }
        alertDialogBuilder.setNegativeButton(
            R.string.no
        ) { dialog, id ->
            // If you choose to not enable the notification listener
            // the app. will not work as expected
        }
        return alertDialogBuilder.create()
    }
}

class MainView(private val controller: MainController): AnkoComponent<MainActivity> {

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        verticalLayout {
            lparams{
                width = matchParent; height = matchParent
            }
            button("Start server") {
                bind("visibility", controller::openStatus) {
                    when(it) {
                        true -> android.view.View.INVISIBLE
                        false -> android.view.View.VISIBLE
                    }
                }
                setOnClickListener {
                    controller.changeServerStatus()
                }
            }.lparams {
                topMargin = dip(15)
                width = wrapContent; height = wrapContent
                gravity = Gravity.CENTER
            }
            textView("Server started") {
                textSize = 20.0f
                textColor = context.getColor(R.color.black)
                bind("visibility", controller::openStatus) {
                    when(it) {
                        true -> android.view.View.VISIBLE
                        false -> android.view.View.INVISIBLE
                    }
                }
            }.lparams {
                width = wrapContent; height = wrapContent
                gravity = Gravity.CENTER
            }
            textView {
                textSize = 16.0f
                textColor = context.getColor(R.color.black)
                bind("visibility", controller::openStatus) {
                    when(it) {
                        true -> android.view.View.VISIBLE
                        false -> android.view.View.INVISIBLE
                    }
                }
                controller::currentIp.addListener {
                    text = "Your ip: $it"
                }
            }.lparams {
                width = wrapContent; height = wrapContent
                gravity = Gravity.CENTER
            }
        }
    }
}