package com.appspot.magtech.utils

import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

fun sendLog(text: String) {
    val message = URLEncoder.encode(text)
    val address = URL("http://192.168.0.105:3001/api/log?text=$message")

    val httpConn = address.openConnection() as HttpURLConnection
    httpConn.connectTimeout = 10 * 1000
    httpConn.readTimeout = 10 * 1000

    val input = httpConn.inputStream
    input.close()
    httpConn.disconnect()
}