package com.appspot.magtech.utils

import android.app.NotificationManager
import android.content.Context
import com.appspot.magtech.ui.connection.FOREGROUND_ID

class SimpleNotificationsWorker(private val context: Context): com.appspot.magtech.application.NotificationsWorker {

    override fun isNotificationExists(id: Int): Boolean {
        var isStarting = false
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        for (n in notificationManager.activeNotifications){
            if (n.id == FOREGROUND_ID){
                isStarting = true
                break
            }
        }
        return isStarting
    }
}